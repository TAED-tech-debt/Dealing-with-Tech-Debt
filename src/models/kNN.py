import pandas as pd
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
import random
from numpy.random import permutation
import math
from sklearn import preprocessing

def KNN(train, test, x_columns, y_column):
    le = preprocessing.LabelEncoder()
    # Converting string labels into numbers.
    train['encoded']=le.fit_transform(train[y_column])
    test['encoded']=le.fit_transform(test[y_column])


    knn = KNeighborsClassifier(n_neighbors=5)

    knn.fit(train[x_columns], train['encoded'])

    prediction = knn.predict(test[x_columns])
    actual = test['encoded']

    # Compute the mean squared error of our predictions.
    mse = (((prediction - actual) ** 2).sum()) / len(prediction)
    return prediction, mse
