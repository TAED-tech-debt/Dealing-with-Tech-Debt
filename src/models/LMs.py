'''
Things implemented here:
    - Linear regression
    - Dataset cleaning by chisq test
'''

import sklearn.linear_model as lm
from sklearn.feature_selection import SelectKBest, chi2


# Fit a linear regression model to determine which variables are more important to predict the target
def fit_lm(features, target, intercept=False):
    return lm.LinearRegression(fit_intercept=intercept).fit(features, target)


# Returns the value for the coefficients of a fitted lm. Higher means more correlated.
def lm_coeffs(features, target, intercept=False):
    return fit_lm(features, target, intercept).coef_


# Returns a reduced dataset with only the k best variables according to the chisq test
# Unfortunately, does not say which variables were originally
def select_best_vars(features, target, k):
    return SelectKBest(chi2, k=k).fit_transform(features, target)
