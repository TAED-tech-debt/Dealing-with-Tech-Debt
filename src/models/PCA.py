import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler


def PCA_calc(df, features, targets, dataframe=False):
    x = df.loc[:, features].values
    y = df.loc[:,targets].values
    x = StandardScaler().fit_transform(x)
    
    pca = PCA(n_components=2)
    principalComponents = pca.fit_transform(x)
    if not dataframe: return principalComponents 
    
    principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])
    finalDf = pd.concat([principalDf, df[targets]], axis = 1)
    return finalDf

def Plot_PCA(df, features, target, legend=False):
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel('Principal Component 1', fontsize = 15)
    ax.set_ylabel('Principal Component 2', fontsize = 15)
    ax.set_title('2 component PCA', fontsize = 20)
    targets = list(pd.unique(df[target]))
    nt = len(targets)
    i = 0
    for targetv in targets:
        indicesToKeep = df[target] == targetv
        ax.scatter(df.loc[indicesToKeep, 'principal component 1']
                   , df.loc[indicesToKeep, 'principal component 2']
                   , cmap = plt.cm.get_cmap('hsv', nt)
                   , s = 10)
        i += 1
    if legend: ax.legend(targets)
    ax.grid()
    return ax

def all(df, features, targets):
    return Plot_PCA(PCA_calc(df,features, targets,dataframe=True),features, targets)
