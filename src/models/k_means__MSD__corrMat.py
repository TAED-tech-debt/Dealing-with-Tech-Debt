import pandas as pd
import numpy as np

from sklearn.manifold import MDS
from sklearn.metrics.pairwise import manhattan_distances, euclidean_distances
from sklearn.cluster import KMeans


# returns a sklearn object with method fit_transform()
def MultiDimScaling(max_iter=150, n_init=2, random_state=0, n_jobs=3):
    return MDS(max_iter=150, n_init=2, random_state=0, n_jobs=3)


# returns an sklearn object with method fit_predict()
def kMeans(n_clusters=5, random_state=41):
    return KMeans(n_clusters=n_clusters, random_state=41)
