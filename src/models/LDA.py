import pandas as pd
import numpy as np
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.model_selection import train_test_split

def LinDisAnalysis(train, test, features, targets, margin = 0):

    # Define the model and apply it to the training data
    lda = LinearDiscriminantAnalysis()
    lda.fit(train[features], train[targets].astype(int))
    
    # Test prediction
    prediction = lda.predict(test[features]).astype(int)

    # Accuracy of the prediction
    target_test = test[targets].to_numpy().astype(int)
    accuracy = sum(prediction == target_test)/len(prediction)
    
    return accuracy[0], lda.coef_


def interpreteCoef(features,coefs):
    res = {}
    for i in range(len(coefs)):
        zipped = zip(features,coefs[i])
        res[i] = sorted(zipped, key = lambda t: -abs(t[1]))
    return res

def plot_LDA(X, y):
    lda = LinearDiscriminantAnalysis()
    transform = lda.fit(X, y).transform(X)
    # Create the LDA plot
    plt.figure()

    lw = 2
    for i in zip(y.unique()):
        plt.scatter(transform[y == i, 0], transform[y == i, 1], alpha=.8,
                    label=i)

    # Add legend to plot in the optimal place (without occlusions)
    plt.legend(loc='best', shadow=False, scatterpoints=1)
    
    plt.title("LDA")

    # Display the LDA plot
    plt.show()