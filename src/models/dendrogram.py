'''
Things implemented here:
    - Dendogram plotting
'''

from scipy.cluster import hierarchy
import matplotlib.pyplot as plt


# Plots a dendogram of the given data
# Parameters are hardcoded here for now
def dendogram(data):
    links = hierarchy.linkage(data, method='single', metric='euclidean', optimal_ordering=False)
    dn = hierarchy.dendrogram(links, truncate_mode='level', p=8)


# Returns a reduced dataset with only the k best variables according to the chisq test
# Unfortunately, does not say which variables were originally
def select_best_vars(features, target, k):
    return SelectKBest(chi2, k=k).fit_transform(features, target)
