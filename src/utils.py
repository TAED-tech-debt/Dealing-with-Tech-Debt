import pandas as pd
import random
from numpy.random import permutation
import math

def TrainTest(df, p=1./3):
    # Randomly shuffle the index of nba.
    random_indices = permutation(df.index)

    # Set a cutoff for how many items we want in the test set (in this case 1/3 of the items)
    test_cutoff = math.floor(len(df)*p)

    # Generate the test set by taking the first 1/3 of the randomly shuffled indices.
    test = df.loc[random_indices[1:test_cutoff]]

    # Generate the train set with the rest of the data.
    train = df.loc[random_indices[test_cutoff:]]
    return train, test

def standardize(df, columns):
    ndf = df.copy()
    ndf[columns]=(df[columns]-df[columns].mean())/df[columns].std()
    return ndf

def PCA_plot(df, features, targets, legend=False):
    # features -> list de columnes
    # target -> list de columnes
    x = df.loc[:, features].values
    y = df.loc[:,[target]].values
    x = StandardScaler().fit_transform(x)
    
    pca = PCA(n_components=2)
    principalComponents = pca.fit_transform(x)
    principalDf = pd.DataFrame(data = principalComponents, columns = ['principal component 1', 'principal component 2'])
    finalDf = pd.concat([principalDf, df[[target]]], axis = 1)
    
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1) 
    ax.set_xlabel('Principal Component 1', fontsize = 15)
    ax.set_ylabel('Principal Component 2', fontsize = 15)
    ax.set_title('2 component PCA', fontsize = 20)
    targets = list(pd.unique(df[target]))
    nt = len(targets)
    i = 0
    for targetv in targets:
        indicesToKeep = finalDf[target] == targetv
        ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                   , finalDf.loc[indicesToKeep, 'principal component 2']
                   , cmap = plt.cm.get_cmap('hsv', nt)
                   , s = 10)
        i += 1
    if legend: ax.legend(targets)
    ax.grid()