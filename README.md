# Dealing with technical debt (TAED2 project)

This is the repository holding the code for the first TAED2 project for team Curmudgeons.

## Team Curmudgeons
* Anxo-Lois Pereira Cánovas
* Verónica Rina Garcia
* Jaume Ros Alonso
* Álvaro Budría Fernández

## Data
The original data comes from [The Technical Debt Dataset](https://github.com/clowee/The-Technical-Debt-Dataset).

## Repository outline
The repository structure follows the [Cookiecutter Data Science](https://drivendata.github.io/cookiecutter-data-science/) design, ommiting unused directories:
* __data/raw__: the dataset without any preprocessing, ready to be used as input to our preprocessing code.
* __data/preprocessed__: the preprocessed dataset, both the regular `warehouse.f` and the standardized `StandardizedWarehouse.f` versions; the latter has been transformed to have every scalar variable set to mean 0 and variance 1.
* __src__: the set Python scripts we have used to perform all the data analysis, we recommend using them from the Analysis notebook provided, but they can be used independently as well.
* __notebooks__: the three iPython notebooks that allow to easily run everything. `Understanding.ipynb` is the code for the data understanding, `Preprocessing.ipynb` is the code for the data preprocessing and `Analysis.ipynb` is the code for the data analysis; note that the last one requires many snippets of Python code found in _src_.
* __requirements.txt__: A list with the all the required Python modules. Note that a running installation of Java is also required to perform the preprocessing (not necessary if you use our already preprocessed version).
